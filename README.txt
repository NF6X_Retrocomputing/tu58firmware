tu58firmware:

A project to disassemble and reverse-engineer the firmware of a DEC
TU58 tape drive for the purpose of gaining understanding of the
drive's communication protocol so that the compatibility of TU58 drive
emulation.

------------------------------------------------------------------------ 

roms/23-294E2-00.bin
roms/23-294E2-00.hex

    ROM data extracted from AMD AM2716DC EPROM on TU58 drive part number
    70-18114, CS Rev. A, from a VAX-11/730 machine. EPROM marked
    23-294E2-00. Sumcheck returned by EPROM reader is 030DEF.

------------------------------------------------------------------------ 

roms/23-089E2-00.bin
roms/23-089E2-00.hex
roms/23-389E2-00.bin
roms/23-389E2-00.hex

   ROM data provided by another collector, with other firmware
   versions.

------------------------------------------------------------------------ 

docs/EK-0TU58-TM-001_TU58tech.pdf
docs/MP01014_TU58-E_Jan80.pdf
docs/MP01063-TU58-D_May80.pdf

    TU58 technical documents from bitsavers.org.

------------------------------------------------------------------------

docs/tu58-schematic.pdf

    Controller board schematic extracted from MP01014_TU58-E_Jan80.pdf
    for convenience.

------------------------------------------------------------------------

docs/hardware-notes.pdf

    Notes about memory map, port map, etc. determined by examination
    of schematic diagram of controller board.

------------------------------------------------------------------------

docs/6402.pdf
docs/8155.pdf

    Datasheets for major components on the controller board.

------------------------------------------------------------------------

disassembly/*.lst
disassembly/*.sh

    Initial disassembled listings of ROMS, and shell scripts to create
    them.

------------------------------------------------------------------------

disassembly/23-389E2.txt

    Commented disassembly of the 23-389E2 ROM contributed by
    Joseph Lang <joe.lang.0000@gmail.com>
    Thanks!

------------------------------------------------------------------------

disassembly/23-089e2.asm

    Another commented disassembly of the 23-089E2 ROM provided
    by Eric Smith <spacewar@gmail.com>
    Thanks!

