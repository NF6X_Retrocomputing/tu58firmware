#!/bin/sh
#
# Script to generate disassembled listing from ROM 23-294E2
# using https://github.com/NF6X/dismantler
#
# First pass invocation based on examination of schematic:
#
# dismantle.py -c 8085 -a \
#   -e 0x0000 -e 0x0024 -e 0x002C -e 0x0034 -e 0x003C \
#   -l 0x0000 RESET \
#   -l 0x0024 RST_FRAME_ERR \
#   -l 0x002C RST_UART_TX \
#   -l 0x0034 RST_UART_RX \
#   -l 0x003C RST_RD_STROBE \
#   -p 0x08 UART \
#   -p 0x20 CSR \
#   -p 0x21 PA \
#   -p 0x22 PB \
#   -p 0x23 PC \
#   -p 0x24 TIMERL \
#   -p 0x25 TIMERH \
#   ../roms/23-294E2-00.bin > 23-294E2-00.lst
#
# Improved invocation below based on iterative examination of
# disassembled listings to identify additional entry points
# which could not be determined automatically.
#
# Notes:
#
# 0x2001 appears to be pointer to one of two RX char handlers.
# Maybe RSP vs. MRSP handlers?
#
# PCHL_* labels and entries are based on constants jumped to via PCHL.
#
# SW_BOOT is jumped to if SW_BOOT_L (PA.6) is low at reset.
#
# INIT1 is called by reset handler. INIT2..INIT4 calls perform
# partial initialization by entering between INIT1 and the RET.
# Guessing that these are just initialization functions, but not positive
# about that yet.
#
# Appears to be a jump table at 0x00D4..0x00EB.
#
# Two bytes of apparent dead code remain at end prior to empty space
# at end of ROM.


dismantle.py -c 8085 -a \
  -e 0x0000 -e 0x0024 -e 0x002C -e 0x0034 -e 0x003C \
  -l 0x0000 RESET \
  -l 0x0024 RST_FRAME_ERR \
  -l 0x002C RST_UART_TX \
  -l 0x0034 RST_UART_RX \
  -l 0x003C RST_RD_STROBE \
  -p 0x08 UART \
  -p 0x20 CSR \
  -p 0x21 PA \
  -p 0x22 PB \
  -p 0x23 PC \
  -p 0x24 TIMERL \
  -p 0x25 TIMERH \
  -l 0x2001 RXHANDLER \
  -e 0x016D -l 0x016D RXHANDLER1 \
  -e 0x01C0 -l 0x01C0 RXHANDLER2 \
  -e 0x01D4 -l 0x01D4 PCHL_01D4 \
  -e 0x01E7 -l 0x01E7 PCHL_01E7 \
  -e 0x01F4 -l 0x01F4 PCHL_01F4 \
  -l 0x00FF SW_BOOT \
  -l 0x0045 INIT1 -l 0x0049 INIT2 -l 0x004C INIT3 -l 0x0053 INIT4 \
  -l 0x00D4 JMPTBL \
  -v 0x00D4 -v 0x00D6 -v 0x00D8 -v 0x00DA -v 0x00DC -v 0x00DE \
  -v 0x00E0 -v 0x00E2 -v 0x00E4 -v 0x00E6 -v 0x00E8 -v 0x00EA \
  ../roms/23-294E2-00.bin > 23-294E2-00.lst

