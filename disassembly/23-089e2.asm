; DEC TU58 tape drive firmware
; Disassembly copyright 2012 Eric Smith <eric@brouhaha.com>
; Note that Z-80 mnemonics are used, despite the hardware having
; an 8085 microprocessor.

; TU58 DECtape II cartridge is a DC100 type cartridge with
; 140 ft of 0.15 in tape
; Tape speed is 30 in/s for read/write, 60 in/s for search
; Bit density is 800 bpi, flux reversal density is 2400 fr/in
; Ratio encoding
; transfer rate is 24 kbytes/s, 41.7 us/bit
; marks (BOT, EOT, IRG) use 1/4 rate (200 bpi, 166.8 us/bit)

; tape format is 512 logical blocks of 512 bytes each
; 2048 physical records of 128 bytes each
; TM figure 1-5 shows blocks with interleave of 2, but is it really
; interleaved by physical records?

; For DC100 information, see:
;   http://www.hp9825.com/html/dc100_tape.html
;   http://www.hp9845.net/9845/tutorials/savetapes/index.html#dc100


flag_data	equ	001h
flag_control	equ	002h	; command
flag_init	equ	004h
flag_boot	equ	008h
flag_continue	equ	010h
flag_xoff	equ	013h

opcode_end	equ	040h

; RIM is the 8085-specific Read Interrupt Mask instruction
; After RIM, A will contain:
;    bit 7:  SID pin
;    bit 6:  RST 7.5 interrupt pending
;    bit 5:  RST 6.5 interrupt pending
;    bit 4:  RST 5.5 interrupt pending
;    bit 3:  interrupt enable flag
;    bit 2:  RST 7.5 interrupt mask
;    bit 1:  RST 6.5 interrupt mask
;    bit 0:  RST 5.5 interrupt mask
rim	macro
	db	020h
	endm

; SIM is the 8085-specific Set Interrupt Mask instruction
; Before SIM, A should contain:
;    bit 7:  SOD pin
;    bit 6:  enable set SOD
;    bit 5:  don't care
;    bit 4:  reset RST 7.5 flip-flop
;    bit 3:  mask set enable
;    bit 2:  RST 7.5 interrupt mask
;    bit 1:  RST 6.5 interrupt mask
;    bit 0:  RST 5.5 interrupt mask
sim	macro
	db	030h
	endm

io_host		equ	008h	; UART or parallel host interface

io_cmd		equ	020h	; 8155 command reg (write only)
io_stat		equ	020h	; 8155 status reg (read only)
io_porta	equ	021h	; 8155 port A (not used for output)
io_portb	equ	022h	; 8155 port B
io_portc	equ	023h	; 8155 port C
io_timer_low	equ	024h	; 8155 timer low
io_timer_high	equ	025h	; 8155 timer high (top two bits are mode)

porta_wrt_permit_l	equ	0
porta_vel_tp		equ	1
porta_b_cart_l		equ	2
porta_a_cart_l		equ	3
porta_run_tp_l		equ	4
porta_oe		equ	5
porta_sw_boot_l		equ	6
porta_mark		equ	7

portb_run_l		equ	0
portb_wrt_enb		equ	1
portb_reverse		equ	2
portb_sel_drv_b_l	equ	3
portb_sel_trk_0_l	equ	4
portb_gain_reduce_l	equ	5
portb_erase_ena_l	equ	6
portb_30_ips		equ	7

portc_boot_l		equ	0
portc_tp3		equ	1	; pin 38 - pulses high after read checksum error
portc_led		equ	2
portc_tp1		equ	3	; pin  1 - pulses high when header record number read OK
portc_tp2		equ	4	; pin  2 - pulses high when header record number read failed

; UART TBRE (22), output 8212 INT* goes to RST 5.5 (02ch)
; UART OE (15) goes to porta_oe
; UART DR (19), input 8212 INT goes to RST 6.5 (034h)
; UART FE (14) goes to TRAP (024h)

; tape RD STROBE goes to RST 7.5 (03ch)


			org	02000h

r2000			ds	1
host_rx_handler		ds	2
r2003			ds	2
r2005			ds	2

cmd_pkt_flag		ds	1	; 0
cmd_pkt_msg_len		ds	1	; 1
cmd_pkt_opcode		ds	1	; 2
cmd_pkt_modifier	ds	1	; 3 - in end packet, success code
cmd_pkt_unit_num	ds	1	; 4
cmd_pkt_switches	ds	1	; 5
cmd_pkt_sequence_num	ds	2	; 6-7 - should be zero
cmd_pkt_byte_count	ds	2	; 8-9
cmd_pkt_block_num	ds	2	; 10-11 - in end packet, summary status
cmd_pkt_checksum	ds	2

r2015			ds	2

data_pkt_flag		ds	1
data_pkt_msg_len	ds	1

data_buffer_size	equ	128

data_buffer		ds	data_buffer_size
data_buffer_last_byte	equ	$-1

data_buffer_checksum	ds	2	; actual checksum bytes from tape


r209b			ds	2
r209d			ds	2

computed_checksum	ds	2

			ds	1	; apparently unused
r20a2			ds	1
r20a3			ds	2
r20a5			ds	2
r20a7			ds	1
r20a8			ds	1
r20a9			ds	2
r20ab			ds	2
r20ad			ds	1
portb_shadow		ds	1
r20af			ds	1
seek_retry_count	ds	1	; counts up to zero
			ds	2	; apparently unused
r20b3			ds	1

success_code		ds	1	; TM page 3-5

summary_status		ds	1	; TM page 3-6
					;    bit 7 = special condition (errors)
					;    bit 6 = transfer error
					;    bit 5 = motion error
					;    bit 4 = logic error
					; (Is there any code that actually sets
					; bits 4, 5, or 6?)

r20b6			ds	1

; rest of RAM is used for stack

ram_end			equ	02100h

	org	00000h

	ld	sp,ram_end-1
	in	a,(io_porta)
	and	(1<<porta_sw_boot_l)
	jp	z,l00e5
	call	s01
	call	s26
	jp	l026c

s01:	xor	a
	ld	(r2000),a
s01a:	ld	hl,l0153
s01b:	ld	(host_rx_handler),hl
	ld	a,04dh		; set SOD=0, enable RST 7.5, 5.5, disable 6.5
	sim
	ei
	jp	s02

rst5:	di
	ld	sp,ram_end-1
	in	a,(io_host)
	ld	a,0fdh

; RST 5.5 interrupt: transmit buffer empty
uart_tbre:
	out	(io_portb),a
	ld	l,005h
	jp	l005d

	nop			;0033	00 	. 

; RST 6.5 interrupt: receive buffer full
uart_dr:
	push	af
	push	hl
	in	a,(io_host)
	ld	hl,(host_rx_handler)
	jp	(hl)

; RST 7.5 interrupt: tape RS STROBE
	ld	a,010h		; reset RST 7.5 flip-flop
	sim
	rim			; get tape RD data
	rlca
	ld	a,b
	rra
	ld	b,a
	ret


s02:	ld	a,0feh
	out	(io_cmd),a
	ld	a,0fdh
	out	(io_portb),a
	in	a,(io_portc)
	or	001h
	out	(io_portc),a
	ld	a,00ch
	ld	(r20ad),a
	xor	a
	ld	(r20a2),a
	ret


l005d:	call	s03
	rim
	and	020h		; RST 6.5 pending?
	jp	z,l005d
	dec	l
	jp	z,l026c
	in	a,(io_host)
	cp	004h
	jp	nz,l005d
	call	s01
	ld	(r20b6),a

l0077:	ld	sp,ram_end-1
	call	s07

	ld	hl,00000h	; success code 000h - OK
				; summary status 000h - no errors
	ld	(success_code),hl

	ld	(r2003),hl
	ld	a,04dh		; set SOD=0, enable RST 7.5, 5.5, disable 6.5
	sim
	ei
l008a:	call	s10
	call	s03

	ld	a,(r20a2)
	and	080h
	jp	z,l008a

	ld	a,(cmd_pkt_flag)	; check command packet flag byte
	cp	002h
	jp	nz,l026c		; not a command packet

	ld	a,(cmd_pkt_opcode)	; check opcode
	cp	max_opcode

	ld	a,-48			; bad opcode
	jp	nc,report_error

	ld	a,(cmd_pkt_opcode)
	ld	hl,opcode_tbl
	rlca
	ld	e,a
	ld	d,000h
	add	hl,de			;00b4	19 	. 
	ld	e,(hl)
	inc	hl
	ld	d,(hl)
	ex	de,hl
	jp	(hl)


opcode_tbl:
	dw	op_nop		;  0  NOP
	dw	op_init		;  1  Init
	dw	op_read		;  2  Read
	dw	op_write	;  3  Write
	dw	op_nop		;  4  (reserved)
	dw	op_position	;  5  Position
	dw	op_nop		;  6  (reserved)
	dw	op_diagnose	;  7  Diagnose
	dw	op_nop		;  8  Get Status
	dw	op_nop		;  9  Set Status
	dw	op_undoc_10	; 10  (reserved) - NOT same as NOP or get status!
	dw	op_nop		; 11  (reserved)
max_opcode	equ	($-opcode_tbl)/2-1


s03:	in	a,(io_porta)
	and	(1<<porta_sw_boot_l)
	ld	a,(r20b6)
	jp	z,l00e2
	or	001h
	ld	(r20b6),a
	ret

l00e2:	and	001h
	ret	z


; boot mode
l00e5:	ld	sp,ram_end-1
	ld	hl,l01a2
	call	s01b
	out	(io_portc),a

	ld	b,0ffh		; wait about one second
	call	delay_long_b

	call	s26

l00f8:	ld	(cmd_pkt_unit_num),a	; received second byte of BOOT sequence, which is drive number
	ld	(r2000),a
	ld	hl,00000h
	ld	(r20a9),hl
l0104:	call	s21
	call	s13
	ld	hl,data_buffer
	ld	c,080h
l010f:	ld	a,(hl)
	call	xmit_byte
	ld	d,a
	ld	a,(host_rx_handler)
	cp	0f8h
	jp	z,l0130
	ld	a,d
	cp	'G'
	jp	z,l014a
	ld	b,002h
	and	0f8h
	xor	030h
	jp	z,l012d
	ld	b,014h
l012d:	call	delay_long_b
l0130:	inc	hl
	dec	c
	jp	nz,l010f
	ld	hl,(r20a9)
	inc	hl
	ld	(r20a9),hl
	ld	a,004h
	cp	l
	jp	nz,l0104
	ld	a,(host_rx_handler)
	cp	0f8h
	jp	nz,l0104


; boot mode done
l014a:	call	s01
	ld	(r20b6),a
	jp	l0077


l0153:	cp	008h		; flag_boot?
	ld	hl,l00f8	; yes, get drive num in second byte
	jp	z,l019f

	cp	013h		; flag_xoff?
	jp	nz,00168h

	ld	a,0ffh
	ld	(r2000),a
	jp	l01a2

l0168:	ld	l,a
	and	0feh
	cp	010h		; flag_continue? (why is LSB masked?)
	jp	nz,l0177

	xor	a
	ld	(r2000),a
	jp	l01a2

l0177:	ld	a,004h		; flag_init?
	cp	l
	jp	z,l022f

	ld	a,(r20a2)
	and	080h
	jp	nz,l026c
	ld	a,l
	ld	(cmd_pkt_flag),a
	ld	(computed_checksum),a
	ld	de,data_buffer
	cp	001h
	jp	z,l019c
	ld	de,cmd_pkt_opcode
	cp	002h		; Read opcode
	jp	nz,l026c
l019c:	ld	hl,l01a6
l019f:	ld	(host_rx_handler),hl

l01a2:	pop	hl
	pop	af
	ei
	ret


l01a6:	ld	c,a
	ld	(cmd_pkt_msg_len),a
	ld	(computed_checksum+1),a
	cp	081h		; packet too long?
	jp	nc,l026c

	ld	b,001h
	ld	hl,l01ba
	jp	l019fh


l01bah:	ld	(de),a
	ld	a,(de)
	call	checksum_byte
	dec	c
	jp	z,l01c7
	inc	de
	jp	l01a2

l01c7:	ld	hl,l01cd
	jp	l019f

l01cd:	ld	hl,computed_checksum
	sub	(hl)
	jp	nz,l026c
	ld	hl,l01da
	jp	l019f

l01da:	ld	hl,computed_checksum+1
	sub	(hl)
	jp	nz,l026c
	ld	hl,(computed_checksum)
	ld	(r2005),hl
	ld	a,(r20a2)
	or	080h
	ld	(r20a2),a
	ld	hl,l0153
	jp	l019fh


; On entry:
;    A = byte to add into checksum
;    B = 0 or 1 for high or low byte, respectively
; On exit:
;    B toggled
checksum_byte:
	push	hl

	ld	l,a		; save byte in A

	ld	a,b		; toggle LSB of B
	xor	001h
	ld	b,a
	jp	nz,l0206	; if high byte, skip low byte processing

l01fe:	ld	a,l		; add byte into low
	ld	hl,computed_checksum
	adc	a,(hl)
	ld	(hl),a
	ld	l,000h		; clear byte and fall into high byte in case of carry

l0206:	ld	a,l
	ld	hl,computed_checksum+1
	adc	a,(hl)
	ld	(hl),a
	ld	l,000h		; clear byte
	jp	c,l01fe		; and go back to low byte if carry

	pop	hl
	ret


; contents of A on entry will be sent to host just before exit
xmit_byte:
	push	af
	push	hl

	ld	l,008h
l0217:	dec	l
	push	bc
	call	z,s13
	pop	bc

	rim
	and	010h			; check TBRE for transmit available
	jp	z,l0217			; no, loop

	ld	a,(r2000)		; in XOFF state?
	inc	a
	jp	z,l0217			; yes, wait for flag_continue received

	pop	hl
	pop	af
	out	(io_host),a
	ret

	
l022f:	ld	sp,ram_end-1
	call	s01
	call	xmit_flag_continue
	jp	l0077


s06:	ld	hl,data_pkt_flag
	ld	(hl),flag_data
	inc	hl
s06a:	ld	d,(hl)
	inc	d
	inc	d

	xor	a			; clear checksum
	ld	(computed_checksum),a
	ld	(computed_checksum+1),a

	ld	b,001h			; start with low byte

	dec	hl

l024e:	ld	a,(hl)
	call	checksum_byte
	ld	a,(hl)
	call	xmit_byte
	inc	hl
	dec	d
	jp	nz,l024e

	ld	hl,computed_checksum
	call	s06z
	inc	hl
s06z:	ld	a,01eh
	call	delay_short_a
	ld	a,(hl)
	call	xmit_byte
	ret

	
; wait for next command packet?
l026c:	di
	ld	sp,ram_end-1
	call	s01
l0273:	ld	flag_init
	call	xmit_byte
	call	s03
	jp	l0273h


s07:	ld	a,(r20a2)
	and	07fh
	ld	(r20a2),a
	ret


op_position:
	call	s25
	call	s08
	call	s11		; read address
	call	s13
	jp	op_nop


s08:	ld	hl,(r20a9)
	ex	de,hl
	ld	hl,portb_shadow
	ld	(hl),0e9h
	ld	a,d
	cp	008h

	ld	a,-2		; partial operation (end of medium)
	jp	nc,report_error

	ld	a,d
	cp	004h
	ccf
	ld	a,e
	rla
	ld	e,a
	ld	a,d
	rla
	and	007h
	ld	d,a
	cp	004h
	jp	c,l02ba
	ld	(hl),0f9h
l02ba:	ld	a,(cmd_pkt_unit_num)
	cp	001h
	jp	z,l02ca
	jp	c,l02d6

	ld	a,-8			; bad unit number
	jp	report_error

l02ca:	ld	a,(hl)
	and	0f7h
	ld	(hl),a
	ld	a,004h
	ld	hl,(r20a5)
	jp	l02db

l02d6:	ld	a,008h
	ld	hl,(r20a3)
l02db:	ld	(r20b3),a
	ld	c,a
	ld	a,(r20ad)
	and	c
	jp	z,l02e8
	ld	l,e
	ld	h,d
l02e8:	call	s09
	ex	de,hl
	ld	(r20a7),hl
	ret


s09:	call	s10
	push	hl
	ld	hl,r20b3
	in	a,(io_porta)
	and	(hl)

	ld	a,-9			; no cartridge
	jp	nz,report_error

	ld	a,(r20ad)
	cpl
	or	(hl)
	cpl
	ld	(r20ad),a
	pop	hl
	ret


s10:	push	hl
	in	a,(io_porta)
	and	(1<<porta_a_cart_l)|(1<<porta_b_cart_l)
	ld	hl,r20ad
	or	(hl)
	ld	(hl),a
	pop	hl
	ret


s11:	ld	a,-8			; up to 8 attempts
	ld	(seek_retry_count),a
l031b:	call	s09
	ld	hl,(r20a7)
	ld	a,h
	and	003h
	ld	h,a
	ld	a,d
	and	003h
	ld	d,a
	ld	a,l
	sub	e
	ld	l,a
	ld	a,h
	sbc	a,d
	ld	h,a
	jp	nc,00358h

l0332:	ld	a,(seek_retry_count)
	inc	a
	ld	(seek_retry_count),a

	ld	a,-32		; seek error (block not found)
	jp	z,report_error

	ld	a,l		; hl = - hl
	cpl
	ld	l,a
	ld	a,h
	cpl
	ld	h,a
	inc	hl

	inc	hl		; hl += 2
	inc	hl

	ld	a,(portb_shadow)
	or	004h
	ld	(portb_shadow),a
	call	s14
	call	s13
	jp	l0372

l0358:	xor	a
	cp	h
	jp	nz,l0367
	cp	l
	jp	z,l0332

	ld	a,004h
	cp	l
	jp	nc,l0372

l0367:	dec	hl
	dec	hl
	dec	hl
	call	s14
	ld	b,00fh
	call	s13a

l0372:	ld	a,0beh		; timer = 190
	out	(io_timer_low),a
	ld	a,0c0h
	out	(io_timer_high),a

	ld	a,0ceh
	out	(io_cmd),a

	ld	a,(portb_shadow)
	or	080h
	and	0fah
	ld	(portb_shadow),a

	ld	b,a
	in	a,(io_portb)
	and	001h
	ld	a,b
	out	(io_portb),a
	ld	b,01eh
	call	nz,delay_long_b
	call	s15
	cp	009h
	jp	p,l04fa
	call	s16

	ld	c,4			; read 4 bytes from tape
	ld	hl,r209b
	call	read_tape_bytes

	ld	a,(r209d)		; is the record number consistent
	ld	hl,r209b		;   with its complement?
	cpl
	cp	(hl)
	jp	nz,l03d3

	ld	a,(r209d+1)
	cpl
	inc	hl
	cp	(hl)
	jp	nz,l03d3

	call	s18
	ld	a,(r20a7)
	cp	e
	jp	nz,l031b
	ld	a,(r20a8)
	cp	d
	jp	nz,l031b

	ld	l,(1<<portc_tp1)	; pulse TP1 for header record number OK
	call	pulse_tp

	ret

; record number inconsistent with its complement
l03d3:	ld	l,(1<<portc_tp2)  	; pulse TP2 for header record number read failure
	call	pulse_tp

	ld	hl,seek_retry_count
	inc	(hl)
	jp	nz,l0372

	ld	a,-32			; seek error (block not found)
	jp	report_error


; pulse a test point on port C
; on entry, L contains the mask for the test point bit
; A changed
pulse_tp:
	in	a,(io_portc)
	xor	l
	out	(io_portc),a
	xor	l
	out	(io_portc),a
	ret


delay_short_a:
	and	0ffh
	nop
	nop
	dec	a
	jp	nz,delay_short_a
	ret


delay_long_b:
	ld	a,060h
	call	delay_short_a
	dec	b
	jp	nz,delay_long_b
	ret


s13:	ld	b,02bh
s13a:	in	a,(io_porta)
	and	(1<<porta_run_tp_l)
	ret	nz

	ld	a,(portb_shadow)
	xor	(1<<portb_reverse)|(1<<portb_run_l)
	out	(io_portb),a
	xor	(1<<porb_run_l)
	out	(io_portb),a

	push	hl
	ld	h,a
l0414:	ld	c,b
	in	a,(io_porta)
	ld	l,a
l0418:	in	a,(io_porta)
	xor	l
	and	(1<<porta_vel_tp)
	jp	nz,l0414
	dec	c
	jp	nz,l0418
	ld	a,(portb_shadow)
	or	(1<<portb_wrt_enb)
	out	(io_portb),a
	pop	hl
	ret


s14:	ld	a,05fh		; timer = 95
	out	(io_timer_low),a
	ld	a,0c0h
	out	(io_timer_high),a

	ld	a,0ceh
	out	(io_cmd),a
	ld	a,(portb_shadow)
	and	07eh
	ld	(portb_shadow),a
	out	(io_portb),a

	ld	b,02dh
	call	delay_long_b

l0448:	call	s15
	cp	004h
	jp	m,l0506
	cp	009h
	jp	p,l04f9
	call	s16
	call	s09
	dec	hl
	xor	a
	cp	h
	jp	nz,l0448
	cp	l
	jp	nz,l0448
	ret


s15:	ld	d,00ch
	ld	e,000h
	ld	b,005h
l046c:	in	a,(io_porta)
	and	090h
	jp	z,0046ch
	and	010h

l0475:	ld	a,-33		; motor stopped
	jp	nz,report_error

l047a:	call	s17
	in	a,(io_porta)
	and	080h
	jp	nz,l048b
	dec	b
	jp	z,s15
	jp	l047ah

l048b:	rim
	and	080h		; tape data bit
	jp	z,l0492
	inc	e
l0492:	dec	d
	jp	nz,l047a
	ld	a,01dh		; reset RST 7.5 FF, enable RST 7.5, 5.5, disable 6.5
	sim
	ld	a,e
	ret


s16:	push	af
	ld	a,(portb_shadow)
	or	001h
	out	(io_portb),a
	nop
	and	0feh
	out	(io_portb),a
l04a8:	ld	e,003h
l04aa:	call	s17
	in	a,(io_porta)
	and	090h
	jp	nz,l04a8
	dec	e
	jp	nz,l04aa
	pop	af
	ret


s17:	ld	a,01dh		; reset RST 7.5 FF, enable RST 7.5, 5.5, disable 6.5
	sim
l04bd:	in	a,(io_porta)
	and	010h
	jp	nz,l0475
	rim			; RST 7.5 pending?
	and	040h
	jp	z,l04bd
	ret


; enter with byte count in C
read_tape_bytes:
	ld	a,01bh		; reset RST 7.5 FF, enable RST 6.5, 5.5, disable 7.5
	sim
l04ce:	ei
	halt
	and	080h
	jp	z,l04ce

	ei
	halt
	ei
	halt
	ei
	halt
l04db:	ei
	halt
	ei
	halt
	ei
	halt
	ei
	halt
	ei
	halt

	ld	(hl),b

	ei
	halt

	dec	c
	jp	z,l04f4

	ei
	halt

	inc	hl

	ei
	halt

	jp	l004db
	
l04f4:	ld	a,00dh		; enable RST 7.5, 5.5, disable 6.5
	sim
	ei
	ret

l04f9:	pop	af
l04fa:	ld	hl,003ffh
	ld	(r209b),hl
	call	s18
	jp	l031b


l0506:	pop	af
	ld	b,028h
	call	delay_long_b
	call	s13
	jp	l0372


s18:	ld	hl,(r209b)
	ex	de,hl
	ld	hl,r20a3
	ld	a,(cmd_pkt_unit_num)
	dec	a
	jp	m,l0523
	ld	hl,r20a5

l0523:	ld	(hl),e
	inc	hl
	ld	(hl),d
	ret

	
s19:	ld	hl,(r20a7)
	ld	(computed_checksum),hl
s19a:	ld	de,data_buffer
s19b:	ld	c,080h

	ld	b,001h		; start with low byte
l0534:	ld	a,(de)
	call	checksum_byte
	inc	de
	dec	c
	ret	z
	jp	l0534


; Note that this subroutine could have been implemented by adding
; the "ld a, 010h" instruction just prior to xmit_byte as another entry
; point.
xmit_flag_continue:
	ld	a,flag_continue
	call	xmit_byte
	ret


s21:	ld	a,0f8h
	ld	(r20af),a
l0549:	call	s08
	ld	a,(r20af)
	and	001h
	jp	nz,l055c
	ld	a,(cmd_pkt_modifier)
	and	001h
	jp	z,l0564
l055c:	ld	a,(portb_shadow)
	and	0dfh
	ld	(portb_shadow),a
l0564:	call	s11			; read address
	ld	a,0a2h
	call	delay_short_a

	ld	c,data_buffer_size+2	; read 130 bytes from tape
	ld	hl,data_buffer
	call	read_tape_bytes

	call	s19

s22:	ld	hl,(computed_checksum)
	ld	a,(data_buffer_checksum)
	cp	l
	jp	nz,l0586
	ld	a,(data_buffer_checksum+1)
	cp	h
	ret	z

l0586:	ld	l,(1<<portc_tp3)	; pulse TP3 for read checksum error
	call	pulse_tp

	call	s13
	ld	hl,(r209b)
	inc	hl
	call	s18
	ld	a,(cmd_pkt_switches)
	and	010h
	jp	nz,l05ac
	ld	hl,r20af
	inc	(hl)

	ld	a,001h			; success but with retries
	ld	(success_code),a

	jp	nz,l0549
	jp	l05b4

l05ac:	ld	a,data_buffer_size
	ld	(data_pkt_msg_len),a
	call	s06

l05b4:	ld	a,-17			; data check error
	jp	report_error


op_write:
	call	s25
l05bc:	call	s07
	call	xmit_flag_continue

	push	de
l05c3:	call	s10
	call	s03
	ld	a,(r20a2)
	and	080h
	jp	z,l05c3
	ld	a,(cmd_pkt_flag)
	cp	001h
	jp	nz,l026c
	pop	de

l05da:	call	s08

	push	hl
	push	de

	ld	a,(cmd_pkt_msg_len)	; is length 128?
	sub	080h
	jp	z,005f2h

	ld	c,a			; no, fill rest of buffer with zeros
	ld	hl,data_buffer_last_byte
	xor	a
l05ec:	ld	(hl),a
	dec	hl
	inc	c
	jp	nz,l05ec

	call	s19
	ld	hl,(computed_checksum)
	ld	(data_buffer_checksum),hl
	ld	hl,00000h
	ld	(cmd_pkt_checksum),hl
	ld	(r2015),hl
	ld	hl,08000h
	ld	(data_pkt_flag),hl
	ld	a,(portb_shadow)
	out	(io_portb),a
	in	a,(io_porta)
	and	001h

	ld	a,-11			; write protected
	jp	nz,report_error

	pop	de
	pop	hl

	call	s11		; read address

; actual writing occurs here
	di
	ld	a,00fh
	call	delay_short_a
	ld	hl,cmd_pkt_checksum
	ld	b,134
	ld	d,data_buffer_size+10	; byte count
	ld	a,(portb_shadow)
	or	(1<<potb_wrt_enb)
	ld	e,a
	and	0ffh^(1<<portb_erase_ena_l)
	out	(io_portb),a
	ld	c,8			; bit count

; write a bit
l0636:	ld	a,0ffh			; 0xc0 would be better, to not affect interrupts, instead of resetting RST7.5 and enabling all
	sim

	ld	a,(hl)			; get one bit from buffer
	rrca
	ld	(hl),a

	or	07fh			; write the bit - should and 0x80, or 0x40
	sim

	dec	c			; byte done?
	jp	z,l0653h

	dec	b			
	jp	nz,l0662h

	ld	a,e
	out	(io_portb),a
	nop

l064b:	ld	a,07fh			; should use 0x40
	sim
	nop
	nop
	jp	l0636

; byte done, advance
l0653:	dec	d		; all bytes done?
	jp	z,l0666		;   yes, finish up
	inc	hl
	ld	a,07fh		; should use 0x40
	ld	c,8		; reset bit count
	sim
	dec	b
	nop
	jp	l0636


l0662:	inc	b
	jp	l064b


l0666:	ld	a,(portb_shadow)	; turn off write enable, erase enable
	out	(io_portb),a
	ld	a,00dh		; enable RST 7.5, 5.5, disable 6.5
	sim
	ei

	call	s13
	ld	a,0ffh
	ld	(r20af),a
	call	s22
	ld	hl,(cmd_pkt_flag)
	ld	(computed_checksum),hl
	call	s19a
	ld	hl,(r2005)
	ld	(data_buffer_checksum),hl
	call	s22
	ld	a,(cmd_pkt_msg_len)
	call	s24
	call	s23
	jp	c,l05bc
	ld	a,(r20a9)
	and	003h
	ld	a,(cmd_pkt_modifier)
	jp	z,l06b7
	cp	000h
	jp	m,l06b7
	ld	hl,00000h
	ld	(cmd_pkt_flag),hl
	ld	(r20ab),hl
	ld	(r2005),hl
	jp	l05dah

l06b7:	cp	001h
	jp	nz,l06c8
	call	s25
l06bf:	call	s21
	call	s23
	jp	c,l06bfh
l06c8:	call	s13
	jp	op_nop


s23:	ld	hl,(r20a9)
	inc	hl
	ld	(r20a9),hl
	ld	hl,(r20ab)
	ld	de,0ff80h
	add	hl,de
	ld	(r20ab),hl
	ret


s24:	ld	hl,r2003	; add A to (r2003)
	add	a,(hl)
	ld	(hl),a
	inc	hl
	ld	a,(hl)
	adc	a,000h
	ld	(hl),a
	ret


s25:	ld	hl,(cmd_pkt_byte_count)
	dec	hl
	ld	(r20ab),hl

	ld	hl,(cmd_pkt_block_num)

	ld	a,(cmd_pkt_modifier)	; "special address mode"? (TM 3.3.1.2)
	cp	000h
	jp	m,l0702		; no

	ld	e,l		; yes, hl *= 8
	ld	d,h
	add	hl,de
	add	hl,de
	add	hl,de

l0702:	ld	a,0f8h
	and	h

	ld	a,-55			; bad block number
	jp	nz,report_error

	ld	(r20a9),hl
	ret


op_read:
	call	s25
l0711:	call	s21
	call	s23
	jp	nc,l0728
	ld	a,data_buffer_size
	ld	(data_pkt_msg_len),a
	call	s24
	call	s06
	jp	l0711

l0728:	call	s13
	ld	a,l
	add	a,081h
	ld	(data_pkt_msg_len),a
	call	s24
	call	s06


op_nop:	ld	hl,flag_control+(10<<8)	; flag_control, length 10
	ld	(cmd_pkt_flag),hl
	ld	a,opcode_end
	ld	(cmd_pkt_opcode),a

	ld	a,(success_code)	; success code
	ld	(cmd_pkt_modifier),a

	xor	a			; summary status
	ld	(cmd_pkt_block_num),a	;   (low byte reserved)
	ld	a,(summary_status)
	ld	(cmd_pkt_block_num+1),a

	ld	hl,(r2003)		; actual byte count for transaction
	ld	(cmd_pkt_byte_count),hl

	ld	b,005h
	call	delay_long_b

	ld	hl,cmd_pkt_msg_len
	call	s06a
	jp	l0077


report_error:
	ld	(success_code),a

	ld	a,080h			; special condition
	ld	(summary_status),a

	ld	sp,ram_end-1
	call	s01a
	jp	op_nop


op_init:
	call	s01a
	jp	op_nop


s26:	ld	a,001h
	out	(io_portc),a

	xor	a			; normal success
	ld	(success_code),a

	ld	b,0c8h
	call	delay_long_b

	ld	hl,00000h
	ld	(computed_checksum),hl
	ex	de,hl
l0791:	call	s19b
	ld	a,008h
	cp	d
	jp	nz,00791h

	ld	hl,(computed_checksum)
	inc	l
	jp	nz,l07c8
	inc	h
	jp	nz,l07c8

	ld	hl,cmd_pkt_byte_count+1
l07a8:	ld	(hl),l
	inc	l
	ld	a,0f0h
	cp	l
	jp	nz,l07a8

l07b0:	dec	l
	ld	a,(hl)
	cp	l
	jp	nz,l07c8
	cp	010h
	jp	nz,l07b0

	call	s02
	ld	a,005h
	out	(io_portc),a

	xor	a			; success code 000h - normal success
					; summary status 000h - no errors
	ld	h,a
	ld	l,a
	jp	l07cbh


l07c8:	ld	hl,080ffh		; success code 0ffh - failed self test
					; summary status 080h - special condition (errors)
l07cb:	ld	(success_code),hl
	ret


op_diagnose:
	call	s26
	jp	op_nop


op_undoc_10:
	ld	hl,l07e4
	call	s06a
	ld	a,018h
	ld	(r2003),a
	jp	op_nop


	db	001h

l07e4:	db	018h,000h,018h,001h
	db	000h,001h,001h,000h
	db	000h,000h,000h,001h
	db	000h,007h,000h,000h
	db	002h,000h,000h,080h
	db	000h,080h,000h,002h
	db	000h,000h

	db	097h,09fh		; this is probably the ROM checksum
