#!/bin/sh
#
# Script to generate disassembled listing from ROM 23-089E2
# using https://github.com/NF6X/dismantler
#
# First pass invocation based on examination of schematic:
#
# dismantle.py -c 8085 -a \
#   -e 0x0000 -e 0x0024 -e 0x002C -e 0x0034 -e 0x003C \
#   -l 0x0000 RESET \
#   -l 0x0024 RST_FRAME_ERR \
#   -l 0x002C RST_UART_TX \
#   -l 0x0034 RST_UART_RX \
#   -l 0x003C RST_RD_STROBE \
#   -p 0x08 UART \
#   -p 0x20 CSR \
#   -p 0x21 PA \
#   -p 0x22 PB \
#   -p 0x23 PC \
#   -p 0x24 TIMERL \
#   -p 0x25 TIMERH \
#   ../roms/23-089E2-00.bin > 23-089E2-00.lst
#
# Improved invocation below based on iterative examination of
# disassembled listings to identify additional entry points
# which could not be determined automatically.
#
# Notes:
#
# This is a quick and dirty initial attempt, possibly with errors.
# See 23-294E2-00.sh for a better example.

dismantle.py -c 8085 -a \
  -e 0x0000 -e 0x0024 -e 0x002C -e 0x0034 -e 0x003C \
  -l 0x0000 RESET \
  -l 0x0024 RST_FRAME_ERR \
  -l 0x002C RST_UART_TX \
  -l 0x0034 RST_UART_RX \
  -l 0x003C RST_RD_STROBE \
  -p 0x08 UART \
  -p 0x20 CSR \
  -p 0x21 PA \
  -p 0x22 PB \
  -p 0x23 PC \
  -p 0x24 TIMERL \
  -p 0x25 TIMERH \
  -e 0x0153 -l 0x0153 PCHL_0153 \
  -e 0x01A6 -l 0x01A6 PCHL_01A6 \
  -e 0x01BA -l 0x01BA PCHL_01BA \
  -e 0x01CD -l 0x01CD PCHL_01CD \
  -e 0x01DA -l 0x01DA PCHL_01DA \
  -l 0x00BA JMPTBL \
  -v 0x00BA -v 0x00BC -v 0x00BE \
  -v 0x00C0 -v 0x00C2 -v 0x00C4 -v 0x00C6 -v 0x00C8 -v 0x00CA -v 0x00CC -v 0x00CE \
  -v 0x00D0 \
  ../roms/23-089E2-00.bin > 23-089E2-00.lst
