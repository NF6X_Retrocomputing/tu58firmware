#!/bin/sh
#
# Script to generate disassembled listing from ROM 23-389E2
# using https://github.com/NF6X/dismantler
#
# First pass invocation based on examination of schematic:
#
# dismantle.py -c 8085 -a \
#   -e 0x0000 -e 0x0024 -e 0x002C -e 0x0034 -e 0x003C \
#   -l 0x0000 RESET \
#   -l 0x0024 RST_FRAME_ERR \
#   -l 0x002C RST_UART_TX \
#   -l 0x0034 RST_UART_RX \
#   -l 0x003C RST_RD_STROBE \
#   -p 0x08 UART \
#   -p 0x20 CSR \
#   -p 0x21 PA \
#   -p 0x22 PB \
#   -p 0x23 PC \
#   -p 0x24 TIMERL \
#   -p 0x25 TIMERH \
#   ../roms/23-389E2-00.bin > 23-389E2-00.lst
#
# Improved invocation below based on iterative examination of
# disassembled listings to identify additional entry points
# which could not be determined automatically.
#
# Notes:
#
# This is a quick and dirty initial attempt, possibly with errors.
# See 23-294E2-00.sh for a better example.

dismantle.py -c 8085 -a \
  -e 0x0000 -e 0x0024 -e 0x002C -e 0x0034 -e 0x003C \
  -l 0x0000 RESET \
  -l 0x0024 RST_FRAME_ERR \
  -l 0x002C RST_UART_TX \
  -l 0x0034 RST_UART_RX \
  -l 0x003C RST_RD_STROBE \
  -p 0x08 UART \
  -p 0x20 CSR \
  -p 0x21 PA \
  -p 0x22 PB \
  -p 0x23 PC \
  -p 0x24 TIMERL \
  -p 0x25 TIMERH \
  -e 0x0174 -l 0x0174 PCHL_0174 \
  -e 0x01C7 -l 0x01C7 PCHL_01C7 \
  -e 0x01DB -l 0x01DB PCHL_01DB \
  -e 0x01EE -l 0x01EE PCHL_01EE \
  -e 0x01FB -l 0x01FB PCHL_01FB \
  -l 0x00D8 JMPTBL \
  -v 0x00D8 -v 0x00DA -v 0x00DC -v 0x00DE \
  -v 0x00E0 -v 0x00E2 -v 0x00E4 -v 0x00E6 -v 0x00E8 -v 0x00EA -v 0x00EC -v 0x00EE \
  ../roms/23-389E2-00.bin > 23-389E2-00.lst
